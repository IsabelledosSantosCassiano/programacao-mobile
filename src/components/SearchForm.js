import React, { useState } from 'react';
import { View, Text, TextInput } from 'react-native';
import styles from '../../Styles.js/SearchFormStyle';
import { Button} from 'react-native-elements';

const SearchForm = ({ onSearch }) => {
  const [date, setDate] = useState('');

  const handleSearch = () => {
    onSearch(date);
  };

  return (

    <View>
      <View style={styles.formContext}>
        <Text style={styles.textTitle}> Fotografia Astronômica do dia</Text>
       <Text style={styles.formLabel}> Informe uma data:</Text>
        <TextInput style={styles.input}
          placeholder="YYYY-MM-DD"
          value={date}
          onChangeText={setDate} />
        <Button title="Procurar" onPress={handleSearch} />
      
      </View>
    </View>

  );
};

export default SearchForm;
