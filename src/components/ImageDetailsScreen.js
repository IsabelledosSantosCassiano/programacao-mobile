import React from 'react';
import { View, Text } from 'react-native';
import styles from '../../Styles.js/ImageCardStyle';

const ImageDetailsScreen = ({ route }) => {
  const { image } = route.params;

  return (
    <View style={styles.container}>
      <Text style={styles.textTitle}> {image.title}</Text>
      <Text style={styles.date}> {image.date}</Text>
      <Text style={styles.detailsContainer} >{image.explanation}</Text>
      {/* Exibir outras informações adicionais da imagem, se necessário */}
    </View>
  );
};

export default ImageDetailsScreen;