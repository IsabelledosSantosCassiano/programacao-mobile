
const API_KEY = '8KSu8hWAtw2Uv0hKdnGPkwP6riQYajaqgKrXhQOa';

export const searchImages = async (date) => {
  const url = `https://api.nasa.gov/planetary/apod?api_key=${API_KEY}&date=${date}`;
  const response = await fetch(url);
  const data = await response.json();
  return Array.isArray(data) ? data : [data];
  
};