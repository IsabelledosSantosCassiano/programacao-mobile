import React from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native'; //Para ImageCard
import styles from '../../Styles.js/ImageCardStyle';

const ImageCard = (props) => {
  const { image, onPress } = props; //extrai propriedades

  return ( //renderiza componentes
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <View style={styles.detailsContainer}>
        <Image source={{ uri: image.url }} style={styles.image} />
        <Text style={styles.title}> {image.title}</Text>
        <Text style={styles.date}> {image.date}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default ImageCard;
