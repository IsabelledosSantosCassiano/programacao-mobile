import React, { useState } from 'react';
import { View, Button, FlatList } from 'react-native';
import SearchForm from './SearchForm';
import ImageCard from './ImageCard';
import ImageDetailsScreen from './ImageDetailsScreen';
import { searchImages } from './Api';


const HomeScreen = ({ navigation }) => {
  const [images, setImages] = useState([]);

  const handleSearch = async (date) => {
    const response = await searchImages(date);
    setImages(response); //atualiza o estado de images
  };
  
  const handleImagePress = (image) => {  //ao pressionar passa para outra view
    navigation.navigate('Detalhes da Imagem', { image });
  };

  return (
    <View>
      <SearchForm onSearch={handleSearch} />

      <FlatList
        data={images}  //pega a lista de imagens e suas propriedades
        keyExtractor={(item) => item.title} 
        renderItem={({ item }) => (
          <ImageCard
            image={item}
            onPress={() => handleImagePress(item)}
          />
        )}
      />
    </View>
  );
};

export default HomeScreen;
