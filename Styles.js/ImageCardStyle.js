import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
      alignItems: 'center',
      marginTop: 30,
    },
    image: {
      width: 200,
      height: 200,
      borderRadius: 10,
      alignItems: 'center',
      marginLeft: 10,
    },
    detailsContainer: {
      margin: 20,
      alignItems: 'flex-center',
      textAlign: 'justify',
      color: "#9400d3",
    },
    title: {
      fontSize: 16,
      fontWeight: 'bold',
      textAlign: 'center',
      marginTop: 30,
    },

    textTitle: {
      fontSize: 24,
      fontWeight: "bold",
  },
    date: {
      marginTop: 5,
      fontSize: 14,
      textAlign: 'center',
    },
  });

  export default styles;
