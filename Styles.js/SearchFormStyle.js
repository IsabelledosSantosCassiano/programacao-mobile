import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    
    boxTitle: {
        alignItems: "center",
        justifyContente: "center",
        padding: 10,
    },
    textTitle: {
        color: "#9400d3",
        fontSize: 24,
        fontWeight: "bold",
        
    },
    formContext: {
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        alignItems: "center",
        marginTop: 30,
    },
    form: {
        width: "100%",
        height: "auto",
        marginTop: 30,
        padding: 10,
    },

   formLabel: {
        color: "#000000",
        fontSize: 18,
        paddingTop: 30,
        paddingBottom: 10,
        //paddingLeft: 80,    
    },
    input: {
        width: "70%",
        borderRadius: 50,
        borderColor: "#cf9bcc",
        backgroundColor: "#ffffff",
        height: 40,
        margin: 20,
        marginTop: 5,
        marginLeft: 30,
        borderWidth: 1,
        paddingLeft: 85,
  
      },
      /*
      buttonProcurar: {
        borderRadius: 50,
        alignItems: "center",
        justifyContent: "center",
        width: "90%",
        backgroundColor: "#ffff",
        paddingTop: 14,
        paddingBottom: 14,
        marginLeft: 12,
        marginTop: 30,
      },*/

});

export default styles;
