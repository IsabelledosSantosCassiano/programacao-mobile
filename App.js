
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './src/components/HomeScreen';
import ImageDetailsScreen from './src/components/ImageDetailsScreen';

const Stack = createStackNavigator();


function App() {
  return (

    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Início" component={HomeScreen} />
        <Stack.Screen name="Detalhes da Imagem" component={ImageDetailsScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );

}
export default App;
